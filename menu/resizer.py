from PIL import Image
import os

files = [f for f in os.listdir('.') if os.path.isfile(f)]
size = 200,200
print(files)
print("asd.jpg"[-4:])

for file in files:
    if file[-4:] == ".jpg" or file[-4:] == ".png":
        t = file[-4:]
        im = Image.open(file)
        im_resized = im.resize(size, Image.ANTIALIAS)
        im_resized.save(file[0:-4] + "_res" + t, "PNG")
